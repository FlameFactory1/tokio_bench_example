# tokio\_bench\_example

## Getting started

```
cargo bench
```

## Getting finished

```
    Finished bench [optimized] target(s) in 0.02s
     Running benches/fib/main.rs (target/release/deps/fib-348a37cbc171c253)
bench_group/fib/30      time:   [24.009 ms 24.025 ms 24.041 ms]
                        change: [-0.0789% +0.0274% +0.1325%] (p = 0.61 > 0.05)
                        No change in performance detected.
Found 1 outliers among 100 measurements (1.00%)
  1 (1.00%) low mild
bench_group/fib_it/30   time:   [2.1378 ns 2.1419 ns 2.1470 ns]
                        change: [+0.0936% +0.2899% +0.5261%] (p = 0.01 < 0.05)
                        Change within noise threshold.
Found 11 outliers among 100 measurements (11.00%)
  3 (3.00%) high mild
  8 (8.00%) high severe
```
