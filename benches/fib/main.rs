use criterion::BenchmarkId;
use criterion::Criterion;
use criterion::{criterion_group, criterion_main};
async fn fib(n: u32) -> u32 {
    match n {
        0 | 1 => 1,
        _ => Box::pin(fib(n - 1)).await + Box::pin(fib(n - 2)).await,
    }
}

fn fib_it(n: u32) -> u32 {
    match n {
        0 | 1 => 1,
        m => {
            let mut a = 1;
            let mut b = 1;
            let mut c;

            let mut i = 1;
            while i < m {
                c = a + b;
                a = b;
                b = c;
                i += 1;
            }
            return b;
        }
    }
}

fn fib_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("bench_group");
    let f = tokio::runtime::Runtime::new().unwrap();
    let n = 30;
    group.bench_with_input(BenchmarkId::new("fib", n), &n, |b, &s| {
        b.to_async(&f).iter(|| fib(s));
    });
    group.bench_with_input(BenchmarkId::new("fib_it", n), &n, |b, &s| {
        b.iter(|| fib_it(s));
    });
    group.finish()
}

criterion_group!(benches, fib_bench);
criterion_main!(benches);
